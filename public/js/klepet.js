// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};



/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var moje = ukaz;
  var ime = ukaz;
  moje = moje.split('');
  if (moje[0] == '@') {
    ukaz = '@';
  }
  
  else {
    var besede = ukaz.split(' ');
    // Izlušči ukaz iz vnosnega besedila
    ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  }
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      var gosti;
      for (var i = 0; i < parametri.length; i++) {
        if(parametri[i].indexOf("Gost") != -1) {
          gosti = parametri[i].split(",");
        }
      }
      for (var i = 0; i < gosti.length; i++) {
        if (parametri) {
          this.socket.emit('sporocilo', {vzdevek: gosti[i], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
        } else {
          sporocilo = 'Neznan ukaz';
        }
      }
      break;
      
    case '@':
      var parametri2 = ime.split('@');
      
      this.socket.emit('sporocilo', {vzdevek: parametri2[1], besedilo: '@'});
      break;
      
    case 'barva':
      besede.shift();
      var besedilo1 = besede.join(' ');
      var parametri1 = besedilo1.split('\"');
      document.getElementById("sporocila").style.color = parametri1;
      document.getElementById("kanal").style.color = parametri1;
      break;
    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = ukaz; //'Neznan ukaz.';
      break;
  }

  return sporocilo;
};